provider "aws" {
  version = ">= 2.63.0"

  region = "eu-central-1"
}

terraform {
  backend "s3" {
    region  = "us-east-1"
    bucket  = "terraform-state-bucket-grace"
    key     = "tfgs.tfstate"
  }
}

module "eks-test-cluster" {
  source = "../infra"
}
